# README #

В активной разработке. Утилита позволяет проводить создание MySQL-запросов через методы.

Пример:


```
#!java

@Override
	public void insert(Connection connection) throws SQLException {
		SeparatedList fields = new SeparatedList();
		fields.addField(ICity.ID, NAME, OWNER, MAP_POINT);
		SeparatedList values = new SeparatedList();
		if (getId() == 0) {
			values.addNull();
		} else {
			values.addInt(getId());
		}
		values.addString(getName());
		values.addInt(getOwner());
		values.addInt(getMapPoint());

		MySQL.insert().into(ICity.TABLE).fields(fields).values(values)
				.toQuery().update(connection);
	}
```


```
#!java

@Override
	public void delete(Connection connection) throws SQLException {
		MySQL.delete().from(TABLE).where(fld(ID).eqInt(getId())).toQuery().update(connection);
	}
```

Поддерживаются Rollback-транзакции, в том числе и с возвратом ключей.  (класс RollbackStatement)