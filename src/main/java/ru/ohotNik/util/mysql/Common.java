package ru.ohotNik.util.mysql;

import java.util.Iterator;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: OkhonchenkoAV
 * Date: 25.07.14
 * Time: 12:45
 */
@Deprecated
public class Common {

	private Common() {

	}

	/**
	 * Преобразит список строк с 1 строку и проставит запяты.
	 *
	 * @param list      список
	 * @param addAmps   добавить кавычки
	 * @param addApostr добавить символы `
	 * @return строка
	 */
	protected static String listOfValues(List<String> list, boolean addAmps, boolean addApostr) {
		if (list.isEmpty()) {
			return "";
		}
		StringBuilder result = new StringBuilder();
		Iterator<String> iterator = list.iterator();

		while (iterator.hasNext()) {
			if (addAmps) {
				result.append('"');
			}
			if (addApostr) {
				result.append('`');
			}
			result.append(iterator.next());
			if (addAmps) {
				result.append('"');
			}
			if (addApostr) {
				result.append('`');
			}
			if (iterator.hasNext()) {
				result.append(',');
			}
		}
		return result.toString();
	}

}
