package ru.ohotNik.util.mysql;

/**
 * Created with IntelliJ IDEA.
 * User: OkhonchenkoAV
 * Date: 14.08.14
 * Time: 14:30
 */
@Deprecated
public class CountQuery {

	private static final String QUERY_TXT = "select count(*) from `%s`";

	public CountStep1 from(String table) {
		StringBuilder query = new StringBuilder();
		query.append(String.format(QUERY_TXT, table));
		return new CountStep1(query);
	}
}
