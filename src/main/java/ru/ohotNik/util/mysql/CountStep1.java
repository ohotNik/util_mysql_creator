package ru.ohotNik.util.mysql;

/**
 * Created with IntelliJ IDEA.
 * User: OkhonchenkoAV
 * Date: 14.08.14
 * Time: 14:59
 */
@Deprecated
public class CountStep1 {

	private StringBuilder query;
	private static final String QUERY_TXT = " where %s ";

	public CountStep1(StringBuilder query) {
		this.query = query;
	}

	public CountStep2 where(FieldEntity eq) {
		query.append(String.format(QUERY_TXT, eq.toString()));
		return new CountStep2(query);
	}

	public int query() {
		return new CountStep2(query).query();
	}
}
