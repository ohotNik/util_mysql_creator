package ru.ohotNik.util.mysql;

/**
 * Created with IntelliJ IDEA.
 * User: OkhonchenkoAV
 * Date: 14.08.14
 * Time: 15:10
 */
@Deprecated
public class CountStep2 {
	private StringBuilder query;

	public CountStep2(StringBuilder query) {
		this.query = query;
	}

  public Statement getStatement() {
    return new Statement(query);
  }

	public int query() {
		throw new UnsupportedOperationException();
	}
}
