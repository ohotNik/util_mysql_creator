package ru.ohotNik.util.mysql;

/**
 * Created with IntelliJ IDEA.
 * User: OkhonchenkoAV
 * Date: 25.07.14
 * Time: 14:11
 */
@Deprecated
public class FieldEntity {
	private String name;
	private OPERATORS oper;
	private String value;

	public FieldEntity(String name) {
		this.name = name;
	}

	@SuppressWarnings("ParameterHidesMemberVariable")
	protected FieldEntity eq(String value) {
		this.value = '\'' + value + '\'';
		oper = OPERATORS.EQUALS;
		return this;
	}

	@Override
	public String toString() {
		StringBuilder res = new StringBuilder();
		res.append(name);
		if (OPERATORS.EQUALS.equals(oper)) {
			res.append('=');
		}
		res.append(value);
		return res.toString();
	}
}
