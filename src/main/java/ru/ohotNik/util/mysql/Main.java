package ru.ohotNik.util.mysql;

import ru.ohotNik.util.mysql.query.common.Function;
import ru.ohotNik.util.mysql.query.select.SelectStep3;

import java.sql.SQLException;

/**
 * Created with IntelliJ IDEA.
 * User: OkhonchenkoAV
 * Date: 25.07.14
 * Time: 12:30
 */
@Deprecated
public class Main {

	private Main() {
	}

	public static void main(String[] args) throws SQLException {

//		MySQL.countAll().from("test").where(Conditions.fld("tst").eq("123")).toQuery().exec(null);

		SelectStep3 limit = MySQL.select().from("map").as("r1").join(MySQL.selectRand(MySQL.selectFunc(Function.MAX, "id").from("map").toQuery()).as("id").toQuery()).as("r2").orderBy("r1.id").limit(1);
		System.out.println();
	}

}
