package ru.ohotNik.util.mysql;

import ru.ohotNik.util.mysql.query.Query;
import ru.ohotNik.util.mysql.query.common.Function;
import ru.ohotNik.util.mysql.query.count.CountQuery1;
import ru.ohotNik.util.mysql.query.delete.DeleteQuery1;
import ru.ohotNik.util.mysql.query.insert.InsertQuery1;
import ru.ohotNik.util.mysql.query.select.SelectStep1;
import ru.ohotNik.util.mysql.query.select.SelectStep2;
import ru.ohotNik.util.mysql.query.select.SelectStep3;
import ru.ohotNik.util.mysql.query.table.TableCreate;
import ru.ohotNik.util.mysql.query.update.UpdateQuery1;

/**
 * Утилитный класс, определяющий какой тип запроса будет выполняться.
 * Created with IntelliJ IDEA.
 * User: OkhonchenkoAV
 * Date: 25.07.14
 * Time: 12:30
 */
public class MySQL {

	private MySQL() {
	}

	public static SelectStep2 select(String... fields) {
		return SelectStep1.selectFields(fields);
	}

	public static CountQuery1 countAll() {
		return new CountQuery1();
	}

	public static CountQuery1 countField(String field) {
		return new CountQuery1(field);
	}

	public static SelectStep2 selectFunc(Function function, String... args) {
		return new SelectStep2(function, args);
	}

	public static SelectStep3 selectRand(Query query) {
		return new SelectStep3("select (" + Function.RND + " (" + query.getSql() + "))");
	}

	public static DeleteQuery1 delete() {
		return new DeleteQuery1();
	}

	public static InsertQuery1 insert() {
		return new InsertQuery1();
	}

	public static UpdateQuery1 update(String table) {
		return new UpdateQuery1(table);
	}

  public static TableCreate createTable(String table) {
    return new TableCreate(table);
  }
}
