package ru.ohotNik.util.mysql;

/**
 * Created with IntelliJ IDEA.
 * User: OkhonchenkoAV
 * Date: 25.07.14
 * Time: 13:34
 */
@Deprecated
public class QStep {

	private QStep() {
	}

	private StringBuilder query;

	public static QStep forQuery(StringBuilder query) {
		QStep qStep = new QStep();
		qStep.query = query;
		return qStep;
	}

	protected QStep where(FieldEntity condition) {
		query.append("where ").append(condition.toString());
		return this;
	}

	protected Query query() {
		return new Query(query);
	}
}
