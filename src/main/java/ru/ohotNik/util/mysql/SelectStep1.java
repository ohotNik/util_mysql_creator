package ru.ohotNik.util.mysql;

import java.util.List;

import static java.util.Arrays.asList;
import static ru.ohotNik.util.mysql.Common.listOfValues;

/**
 * Created with IntelliJ IDEA.
 * User: OkhonchenkoAV
 * Date: 25.07.14
 * Time: 12:32
 *
 * @deprecated
 */
public class SelectStep1 {

	@SuppressWarnings("MismatchedQueryAndUpdateOfStringBuilder")
	private final StringBuilder query = new StringBuilder("select ");

	private SelectStep1() {
	}

	protected static SelectStep1 selectFields(String... fields) {
		List<String> list = asList(fields);
		SelectStep1 step1 = new SelectStep1();
		if (list.isEmpty()) {
			step1.query.append("* ");
		} else {
			step1.query.append('(');
			step1.query.append(
					listOfValues(list, false, true));
			step1.query.append(") ");
		}

		return step1;
	}

	protected QStep from(String table) {
		return SelectStep2.selectFromTable(table, query);
	}
}
