package ru.ohotNik.util.mysql;

/**
 * Created with IntelliJ IDEA.
 * User: OkhonchenkoAV
 * Date: 25.07.14
 * Time: 13:21
 */
@Deprecated
public class SelectStep2 {

	private SelectStep2() {
	}

	protected static QStep selectFromTable(String table, StringBuilder query) {
		query.append("from ").append(table).append(' ');
		return QStep.forQuery(query);
	}

}
