package ru.ohotNik.util.mysql;

import java.io.Serializable;

/**
 * Created by strun_000 on 15.08.2014.
 */
@Deprecated
public class Statement implements Serializable {
  private StringBuilder query;

  public Statement(CountStep2 cs2) {

  }

  public Statement(SelectStep2 step2) {

  }

  public Statement(StringBuilder query) {
    this.query = query;
  }

  @Override
  public String toString() {
    return query == null ? "" : query.toString();
  }

  @Override
  public boolean equals(Object obj) {
    if (!(obj instanceof Statement)) {
      return false;
    }
    if (query == null) {
      return false;
    }
    return query.toString().equals(obj.toString());
  }
}
