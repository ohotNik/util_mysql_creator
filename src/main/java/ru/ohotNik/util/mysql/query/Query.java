package ru.ohotNik.util.mysql.query;

import ru.ohotNik.util.mysql.transaction.Response;
import ru.ohotNik.util.mysql.transaction.Transactions;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Created with IntelliJ IDEA.
 * User: OkhonchenkoAV
 * Date: 01.09.14
 * Time: 13:11
 */
@SuppressWarnings("UnusedDeclaration")
public class Query {

	private final String query;

	public Query(String query) {
		this.query = query;
	}

	@Override
	public String toString() {
		return "Query:\n" +
				"query=" + query;
	}

	public String getSql() {
		return query;
	}

	public Response exec(Connection connection) throws SQLException {
		return Transactions.exec(this, connection);
	}

	public void update(Connection connection) throws SQLException {
		Transactions.update(this, connection);
	}

	public void exec(Statement stmt) throws SQLException {
		Transactions.execStatement(stmt, this);
	}
}
