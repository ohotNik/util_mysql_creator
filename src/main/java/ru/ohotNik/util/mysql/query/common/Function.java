package ru.ohotNik.util.mysql.query.common;

/**
 * Функции MySQL.
 * <p/>
 * Created by       OkhonchenkoAV
 * Date             29.09.14
 * Time             16:29
 */
public enum Function {
	MAX("MAX(%s)"),
	RND("RAND() *");

	private String txt;

	Function(String txt) {
		this.txt = txt;
	}

	@Override
	public String toString() {
		return txt;
	}
}
