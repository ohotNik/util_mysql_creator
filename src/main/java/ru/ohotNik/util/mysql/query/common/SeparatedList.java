package ru.ohotNik.util.mysql.query.common;

import com.google.common.base.Joiner;

import java.util.Collections;
import java.util.List;

import static com.google.common.collect.Lists.newArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: OkhonchenkoAV
 * Date: 08.09.14
 * Time: 18:08
 */
@SuppressWarnings("UnusedDeclaration")
public class SeparatedList {
	private static final char DEFAULT_SEPARATOR = ',';
	public static final String CUR_TIME = "CURRENT_TIMESTAMP";
	private final char separator;
	private final List<String> values = newArrayList();

	public SeparatedList() {
		separator = DEFAULT_SEPARATOR;
	}

	public SeparatedList(char separator) {
		this.separator = separator;
	}

	public void addField(String... fieldName) {
		for (String field : fieldName) {
			//noinspection MagicCharacter
			append('`' + field + '`');
		}
	}

	public void addField(String fieldName) {
		//noinspection MagicCharacter
		append('`' + fieldName + '`');
	}

	public void addString(String str) {
		//noinspection MagicCharacter
		append('"' + str + '"');
	}

	public void addInt(Integer integer) {
		append(integer.toString());
	}

	@SuppressWarnings("UnusedDeclaration")
	public void addLong(Long aLong) {
		append(aLong.toString());
	}

	public void addNull() {
		append("null");
	}

	public void addCurrentTimestamp() {
		append(CUR_TIME);
	}

	@Override
	public String toString() {
		return Joiner.on(DEFAULT_SEPARATOR).join(values);
	}

	public List<String> toList() {
		return Collections.unmodifiableList(values);
	}

	@SuppressWarnings("HardCodedStringLiteral")
	public void addBool(Boolean bool) {
		if (bool) {
			append("TRUE");
		} else {
			append("FALSE");
		}
	}

	private void append(String str) {
		values.add(str);
	}

	public boolean isEmpty() {
		return values.isEmpty();
	}

}
