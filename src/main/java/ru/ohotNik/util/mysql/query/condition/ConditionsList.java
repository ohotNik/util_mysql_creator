package ru.ohotNik.util.mysql.query.condition;

import java.util.List;

import static com.google.common.collect.Lists.newArrayList;

/**
 * Some class description.
 * <p/>
 * Created by       OkhonchenkoAV
 * Date             01.10.14
 * Time             17:45
 */
public class ConditionsList {

	private List<Field> fieldList = newArrayList();

	public ConditionsList(Field... fields) {
		for (Field f : fields) {
			fieldList.add(f);
		}
	}

	@Override
	public String toString() {
		StringBuilder result = new StringBuilder();
		for (Field f : fieldList) {
			result.append(f.toString());
		}

		return result.toString();
	}

	public ConditionsList and(Field field) {
		field.setOperation(Operator.AND);
		fieldList.add(field);
		return this;
	}

}
