package ru.ohotNik.util.mysql.query.condition;

/**
 * Created with IntelliJ IDEA.
 * User: OkhonchenkoAV
 * Date: 01.09.14
 * Time: 14:55
 */
@SuppressWarnings("UnusedDeclaration")
public class Field {
	private final String name;
	private String value;
	private String delim = "=";
	private Operator operation;

	public Field(String name) {
		this.name = name;
	}

	@SuppressWarnings("ParameterHidesMemberVariable")
	public Field eq(String value) {
		//noinspection MagicCharacter
		this.value = '"' + value + '"';
		return this;
	}

	public Field eqInt(Integer val) {
		value = val.toString();
		return this;
	}

	public Field eqLong(Long val) {
		value = val.toString();
		return this;
	}

	public Field eq(Object o) {
		//noinspection MagicCharacter
		value = '"' + o.toString() + '"';
		return this;
	}

	public String getName() {
		return name;
	}

	public String getValue() {
		return value;
	}

	public String getDelim() {
		return delim;
	}

	public Field notLess(String val) {
		value = val;
		delim = ">=";
		return this;
	}

	public ConditionsList and(Field field) {
		field.setOperation(Operator.AND);
		return new ConditionsList(this, field);
	}

	@Override
	public String toString() {
		StringBuilder res = new StringBuilder();
		if (getOperation() != null) {
			res.append(' ').append(getOperation().toString());
		}
		res.append(' ');
		res.append(name);
		res.append(delim);
		res.append(value);
		return res.toString();
	}

	public Operator getOperation() {
		return operation;
	}

	public void setOperation(Operator operation) {
		this.operation = operation;
	}
}
