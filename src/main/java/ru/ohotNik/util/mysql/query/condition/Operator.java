package ru.ohotNik.util.mysql.query.condition;

/**
 * Операторы объединения операций.
 * <p/>
 * Created by       OkhonchenkoAV
 * Date             01.10.14
 * Time             17:41
 */
public enum Operator {
	AND("and");

	private String operator;

	Operator(String operator) {
		this.operator = operator;
	}

	@Override
	public String toString() {
		return operator;
	}
}
