package ru.ohotNik.util.mysql.query.count;

/**
 * Created with IntelliJ IDEA.
 * User: OkhonchenkoAV
 * Date: 01.09.14
 * Time: 13:16
 */
public class CountQuery1 {

	private static final char WRAP_CHAR = '`';
	private String query;
	private static final String COUNT = "select count(%s) ";

	public CountQuery1() {
		query = String.format(COUNT, "*");
	}

	public CountQuery1(String field) {
		query = String.format(COUNT, WRAP_CHAR + field + WRAP_CHAR);
	}

	public CountQuery2 from(String table) {
		return new CountQuery2(query, table);
	}
}
