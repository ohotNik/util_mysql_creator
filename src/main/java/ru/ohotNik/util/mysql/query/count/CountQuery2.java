package ru.ohotNik.util.mysql.query.count;

import ru.ohotNik.util.mysql.query.Query;
import ru.ohotNik.util.mysql.query.condition.Field;

/**
 * Created with IntelliJ IDEA.
 * User: OkhonchenkoAV
 * Date: 01.09.14
 * Time: 13:29
 */
public class CountQuery2 {
	private String query;

	public CountQuery2(String query, String table) {
		this.query = query;
		this.query += "from `" + table + "` "; //NON-NLS
	}

	public Query toQuery() {
		return new Query(query);
	}


	public CountQuery3 where(Field field) {
		//noinspection MagicCharacter
		query += "where `" + field.getName() + "`=" + field.getValue() + ' '; //NON-NLS
		return new CountQuery3(query);
	}
}
