package ru.ohotNik.util.mysql.query.count;

import ru.ohotNik.util.mysql.query.Query;

/**
 * Created with IntelliJ IDEA.
 * User: OkhonchenkoAV
 * Date: 01.09.14
 * Time: 15:07
 */
public class CountQuery3 {
	private final String query;

	public CountQuery3(String query) {
		this.query = query;
	}

	public Query toQuery() {
		return new Query(query);
	}
}
