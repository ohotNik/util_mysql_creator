package ru.ohotNik.util.mysql.query.delete;

import ru.ohotNik.util.mysql.query.Query;
import ru.ohotNik.util.mysql.query.condition.Field;

/**
 * Created with IntelliJ IDEA.
 * User: OkhonchenkoAV
 * Date: 08.09.14
 * Time: 17:46
 */
public class DeleteQuery2 {

	private final String query;

	public DeleteQuery2(String table) {
		//noinspection MagicCharacter
		query = "delete from `" + table + '`'; //NON-NLS
	}

	public Query toQuery() {
		return new Query(query);
	}


	public DeleteQuery3 where(Field field) {
		//noinspection MagicCharacter,DuplicateStringLiteralInspection
		return new DeleteQuery3(query + " where `" + field.getName() + "`=" + field.getValue() + ' '); //NON-NLS
	}
}
