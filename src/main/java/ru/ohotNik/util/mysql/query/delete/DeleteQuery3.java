package ru.ohotNik.util.mysql.query.delete;

import ru.ohotNik.util.mysql.query.Query;

/**
 * Created with IntelliJ IDEA.
 * User: OkhonchenkoAV
 * Date: 08.09.14
 * Time: 17:49
 */
public class DeleteQuery3 {
	private final String query;

	public DeleteQuery3(String query) {
		this.query = query;
	}

	public Query toQuery() {
		return new Query(query);
	}
}
