package ru.ohotNik.util.mysql.query.insert;

import ru.ohotNik.util.mysql.query.common.SeparatedList;

/**
 * Created with IntelliJ IDEA.
 * User: OkhonchenkoAV
 * Date: 08.09.14
 * Time: 18:24
 */
public class InsertQuery2 {

	private final String query;

	public InsertQuery2(String table) {
		query = "insert into `" + table + '`';
	}

	public InsertQuery3 fields(SeparatedList list) {
		return new InsertQuery3(query, list);
	}
}
