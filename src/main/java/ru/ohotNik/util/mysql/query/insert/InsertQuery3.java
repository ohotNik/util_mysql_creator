package ru.ohotNik.util.mysql.query.insert;

import ru.ohotNik.util.mysql.query.common.SeparatedList;

/**
 * Created with IntelliJ IDEA.
 * User: OkhonchenkoAV
 * Date: 08.09.14
 * Time: 18:26
 */
public class InsertQuery3 {

	private final String query;

	public InsertQuery3(String query, SeparatedList list) {
		//noinspection MagicCharacter
		this.query = query + '(' + list + ')';
	}

	public InsertQuery4 values(SeparatedList list) {
		return new InsertQuery4(query, list);
	}
}
