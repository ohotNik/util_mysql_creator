package ru.ohotNik.util.mysql.query.insert;

import ru.ohotNik.util.mysql.query.Query;
import ru.ohotNik.util.mysql.query.common.SeparatedList;

/**
 * Created with IntelliJ IDEA.
 * User: OkhonchenkoAV
 * Date: 08.09.14
 * Time: 18:31
 */
public class InsertQuery4 {

	private final String query;

	public InsertQuery4(String query, SeparatedList list) {
		//noinspection MagicCharacter
		this.query = query + " VALUES (" + list + ')'; //NON-NLS
	}

	public Query toQuery() {
		return new Query(query);
	}


}
