package ru.ohotNik.util.mysql.query.select;

import ru.ohotNik.util.mysql.query.common.SeparatedList;

/**
 * Created with IntelliJ IDEA.
 * User: OkhonchenkoAV
 * Date: 11.09.14
 * Time: 18:21
 */
public class SelectStep1 {

	public static SelectStep2 selectFields(String... fields) {
		String query;

		if (fields.length == 0) {
			query = "select * ";
		} else {
			SeparatedList separatedList = new SeparatedList();
			separatedList.addField(fields);
			query = "select " + separatedList.toString() + ' ';
		}
		return new SelectStep2(query);

	}

}
