package ru.ohotNik.util.mysql.query.select;

import ru.ohotNik.util.mysql.query.common.Function;

/**
 * Created with IntelliJ IDEA.
 * User: OkhonchenkoAV
 * Date: 11.09.14
 * Time: 18:25
 */
public class SelectStep2 {

	private final String query;

	public SelectStep2(String query) {
		this.query = query;
	}

	public SelectStep2(Function function, String... args) {
		query = "select " + String.format(function.toString(), args) + ' ';
	}

	public SelectStep3 from(String table) {
		return new SelectStep3(query + "from `" + table + '`');
	}
}
