package ru.ohotNik.util.mysql.query.select;

import ru.ohotNik.util.mysql.query.Query;
import ru.ohotNik.util.mysql.query.condition.ConditionsList;
import ru.ohotNik.util.mysql.query.condition.Field;

/**
 * Created with IntelliJ IDEA.
 * User: OkhonchenkoAV
 * Date: 11.09.14
 * Time: 18:30
 */
@SuppressWarnings("HardCodedStringLiteral")
public class SelectStep3 {

	private String query;

	public SelectStep3(String query) {
		this.query = query;
	}

	public Query toQuery() {
		return new Query(query);
	}

	public SelectStep3 where(Field field) {
		//noinspection MagicCharacter,DuplicateStringLiteralInspection
		return new SelectStep3(query + " where " + field.toString());
	}

	public SelectStep3 where(ConditionsList fields) {
		//noinspection DuplicateStringLiteralInspection
		return new SelectStep3(query + " where " + fields.toString());
	}

	public SelectStep3 as(String unName) {
		return new SelectStep3(query + " as " + unName);
	}

	public SelectStep3 join(Query joinedQuery) {
		String querySql = joinedQuery.getSql();
		return new SelectStep3(query + " JOIN (" + querySql + ')');
	}

	public SelectStep3 orderBy(String fieldName) {
		return new SelectStep3(query + " order by " + fieldName);
	}

	public SelectStep3 limit(int limit) {
		return new SelectStep3(query + " limit " + limit);
	}
}
