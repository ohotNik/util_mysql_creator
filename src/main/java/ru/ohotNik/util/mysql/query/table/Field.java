package ru.ohotNik.util.mysql.query.table;

/**
 * Created by ohotNik
 * Date : 08.10.14
 * Time : 21:23
 * Description :
 */
public class Field {

  private Type type;
  private int size;

  public int getSize() {
    return size;
  }

  public void setSize(int size) {
    this.size = size;
  }

  public Type getType() {
    return type;
  }

  public void setType(Type type) {
    this.type = type;
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append(type);
    builder.append('(').append(getSize()).append(") ");

    return builder.toString();
  }

}
