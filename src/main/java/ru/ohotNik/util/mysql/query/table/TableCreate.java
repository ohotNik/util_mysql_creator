package ru.ohotNik.util.mysql.query.table;

/**
 * Created by ohotNik
 * Date : 08.10.14
 * Time : 20:59
 * Description :
 */
public class TableCreate {

  private final StringBuilder builder = new StringBuilder();

  @SuppressWarnings({"MagicCharacter", "HardCodedStringLiteral"})
  public TableCreate(String table) {
    builder.append("CREATE TABLE IF NOT EXISTS `").append(table).append('`');
  }

  public TableCreate1 fields() {
    throw new UnsupportedOperationException();
  }

}
