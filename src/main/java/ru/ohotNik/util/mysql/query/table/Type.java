package ru.ohotNik.util.mysql.query.table;

/**
 * Created by ohotNik
 * Date : 08.10.14
 * Time : 21:24
 * Description :
 */
public enum Type {
  INT("int"),
  VARCHAR("varchar");

  private String text;

  Type(@SuppressWarnings("ParameterHidesMemberVariable") String text) {
    this.text = text;
  }

  @Override
  public String toString() {
    return text;
  }

}
