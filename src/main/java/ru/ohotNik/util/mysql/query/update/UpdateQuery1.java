package ru.ohotNik.util.mysql.query.update;

import com.google.common.base.Joiner;
import ru.ohotNik.util.mysql.query.common.SeparatedList;

import java.util.List;

import static com.google.common.collect.Lists.newArrayList;

/**
 * Обновление данных.
 * <p/>
 * Created by       OkhonchenkoAV
 * Date             30.09.14
 * Time             15:22
 */
public class UpdateQuery1 {

	private final StringBuilder query;

	public UpdateQuery1(String table) {
		query = new StringBuilder("update `").append(table).append("` ");
	}

	public UpdateQuery2 set(SeparatedList fields, SeparatedList values) {
		query.append("set ");
		List<String> fieldsList = fields.toList();
		List<String> valuesList = values.toList();
		if (fieldsList.size() != valuesList.size()) {
			throw new IllegalArgumentException(fieldsList + "/" + valuesList);
		}
		List<String> sets = newArrayList();
		for (int i = 0; i < fieldsList.size(); i++) {
			sets.add(fieldsList.get(i) + '=' + valuesList.get(i));
		}
		return new UpdateQuery2(query.append(Joiner.on(',').join(sets)).toString());
	}
}
