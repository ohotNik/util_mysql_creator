package ru.ohotNik.util.mysql.query.update;

import ru.ohotNik.util.mysql.query.Query;
import ru.ohotNik.util.mysql.query.condition.Field;

/**
 * Some class description.
 * <p/>
 * Created by       OkhonchenkoAV
 * Date             30.09.14
 * Time             16:06
 */
public class UpdateQuery2 {

	private String query;

	public UpdateQuery2(String query) {
		this.query = query;
	}

	public UpdateQuery2 where(Field field) {
		return new UpdateQuery2(query + " where " + field.getName()
				+ field.getDelim() + field.getValue() + ' ');
	}

	public Query toQuery() {
		return new Query(query);
	}

}
