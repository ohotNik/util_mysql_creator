package ru.ohotNik.util.mysql.transaction;

import java.util.List;

import static com.google.common.collect.Lists.newArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: OkhonchenkoAV
 * Date: 01.09.14
 * Time: 15:32
 */
public class Response {

	private List<String> names = newArrayList();
	private List<String> types = newArrayList();
	private List<List<String>> rows = newArrayList();

	/**
	 * Создаст объект для работы с ответом из БД на селект.
	 *
	 * @param rs ResultSet из java.sql
	 */
	@SuppressWarnings({"HardCodedStringLiteral", "UseOfSystemOutOrSystemErr"})
	public Response(List<List<String>> rs) {
		System.out.println("Response : ");
		names = rs.get(0);
		System.out.println("-names : " + names);
		types = rs.get(1);
		System.out.println("-types : " + types);
		for (int i = 2; i < rs.size(); i++) {
			rows.add(rs.get(i));
		}
		System.out.println("-rows : " + rows.size());
	}

	/**
	 * Список имен столбцов.
	 *
	 * @return -
	 */
	public List<String> getNamesList() {
		return newArrayList(names);
	}

	/**
	 * Список типов столбцов.
	 *
	 * @return -
	 */
	public List<String> getTypesList() {
		return newArrayList(types);
	}

	/**
	 * Количество столбцов.
	 *
	 * @return -
	 */
	public int rowCount() {
		return rows.size();
	}

	/**
	 * столбец по номеру.
	 *
	 * @param index индекс
	 * @return -
	 */
	public List<String> getRow(int index) {
		return newArrayList(rows.get(index));
	}

	/**
	 * проверка не пуст ли ответ.
	 *
	 * @return да/нет
	 */
	public boolean isEmpty() {
		return rowCount() == 0;
	}
}
