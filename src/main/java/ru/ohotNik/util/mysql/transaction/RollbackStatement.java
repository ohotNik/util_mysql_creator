package ru.ohotNik.util.mysql.transaction;

import ru.ohotNik.util.mysql.query.Query;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * ОТкатываемый список процедур.
 * <p/>
 * Created by       OkhonchenkoAV
 * Date             24.10.14
 * Time             18:54
 */
@SuppressWarnings("UnusedDeclaration")
public class RollbackStatement {

	private final Statement statement;
	private final Connection connection;

	public RollbackStatement(Connection connection) throws SQLException {
		this.connection = connection;
		connection.setAutoCommit(false);
		statement = connection.createStatement();
	}

	public ResultSet executeWithKeys(String sql) throws SQLException {
		//noinspection JDBCExecuteWithNonConstantString
		statement.execute(sql, Statement.RETURN_GENERATED_KEYS);
		//noinspection JDBCResourceOpenedButNotSafelyClosed
		return statement.getGeneratedKeys();
	}

	public void execute(String sql) throws SQLException {
		//noinspection JDBCExecuteWithNonConstantString
		statement.execute(sql);
	}

	public ResultSet executeWithKeys(Query query) throws SQLException {
		return executeWithKeys(query.getSql());
	}

	public void execute(Query query) throws SQLException {
		execute(query.getSql());
	}

	public void commit() throws SQLException {
		connection.commit();
	}

}
