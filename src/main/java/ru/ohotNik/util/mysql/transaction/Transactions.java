package ru.ohotNik.util.mysql.transaction;

import ru.ohotNik.util.mysql.query.Query;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import static com.google.common.collect.Lists.newArrayList;
import static java.sql.Statement.RETURN_GENERATED_KEYS;

/**
 * Created with IntelliJ IDEA.
 * User: OkhonchenkoAV
 * Date: 01.09.14
 * Time: 15:35
 */
public class Transactions {

	private Transactions() {
	}

	/**
	 * 0 - тип.
	 * 1 - имя.
	 * далее списки со значениями.
	 *
	 * @return -.
	 */
	@SuppressWarnings("UseOfSystemOutOrSystemErr")
	public static Response exec(Query query, Connection connection) throws SQLException {
		String sql = query.getSql();
		System.out.println(query.toString());
		Response res = null;
		PreparedStatement statement = null;
		ResultSet rs = null;
		try {
			//noinspection JDBCResourceOpenedButNotSafelyClosed,JDBCPrepareStatementWithNonConstantString
			statement = connection.prepareStatement(sql);
			rs = statement.executeQuery();
			res = convertResultSetToResponse(rs);
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException e) {
					System.err.println(e.getMessage());
				}
			}
			if (statement != null) {
				try {
					statement.close();
				} catch (SQLException e) {
					System.err.println(e.getMessage());
				}
			}
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					System.err.println(e.getMessage());
				}
			}
		}
		return res;
	}

	@SuppressWarnings("UseOfSystemOutOrSystemErr")
	public static int update(Query query, Connection connection) throws SQLException {
		String sql = query.getSql();
		System.out.println(query.toString());
		int res = -1;
		PreparedStatement statement = null;
		try {
			//noinspection JDBCResourceOpenedButNotSafelyClosed,JDBCPrepareStatementWithNonConstantString
			statement = connection.prepareStatement(sql);
			res = statement.executeUpdate();
		} finally {
			if (statement != null) {
				try {
					statement.close();
				} catch (SQLException e) {
					System.err.println(e.getMessage());
				}
			}
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					System.err.println(e.getMessage());
				}
			}
		}
		return res;
	}

	@SuppressWarnings("UnusedDeclaration")
	public static Response execStatement(Statement stmt, Query query) throws SQLException {
		//noinspection JDBCExecuteWithNonConstantString
		stmt.execute(query.getSql(), RETURN_GENERATED_KEYS);
		return convertResultSetToResponse(stmt.getGeneratedKeys());
	}

	private static Response convertResultSetToResponse(ResultSet rs) throws SQLException {
		ResultSetMetaData metaData = rs.getMetaData();

		List<String> names = newArrayList();
		List<String> types = newArrayList();
		for (int i = 1; i <= metaData.getColumnCount(); i++) {
			names.add(metaData.getColumnName(i));
			types.add(metaData.getColumnTypeName(i));
		}
		List<List<String>> args = newArrayList();
		args.add(names);
		args.add(types);
		//noinspection TooBroadScope
		List<String> row;
		while (rs.next()) {
			row = newArrayList();
			for (int i = 1; i <= metaData.getColumnCount(); i++) {
				row.add(rs.getString(i));
			}
			args.add(row);
		}
		return new Response(args);
	}


}
