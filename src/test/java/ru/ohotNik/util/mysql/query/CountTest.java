package ru.ohotNik.util.mysql.query;

import org.junit.Test;
import ru.ohotNik.util.mysql.MySQL;

import static org.junit.Assert.assertTrue;
import static ru.ohotNik.util.mysql.query.condition.Conditions.fld;

/**
 * Created with IntelliJ IDEA.
 * User: OkhonchenkoAV
 * Date: 01.09.14
 * Time: 13:25
 */
@SuppressWarnings("UseOfSystemOutOrSystemErr")
public class CountTest {

	@Test
	public void testCountAll() {
		System.out.println("--testCountAll--");
		String queryTxt =
				MySQL.countAll().from("test").toQuery().toString();
		System.out.println(queryTxt);
		assertTrue("Проверьте правильность запроса", queryTxt.contains("query=select count(*) from `test`"));
	}

	@Test
	public void testCountField() {
		System.out.println("--testCountField--");
		String queryTxt =
				MySQL.countField("field").from("test").toQuery().toString();
		System.out.println(queryTxt);
		assertTrue("Проверьте правильность запроса", queryTxt.contains("query=select count(`field`) from `test`"));
	}

	@Test
	public void testCountFieldWithCond() {
		System.out.println("--testCountFieldWithCond--");
		String queryTxt =
				MySQL.countField("field").from("test")
						.where(fld("field2").eq("value"))
						.toQuery().toString();
		System.out.println(queryTxt);
		assertTrue("Проверьте правильность запроса", queryTxt.contains("query=select count(`field`) from `test` where `field2`=\"value\""));
	}

}
