package ru.ohotNik.util.mysql.query;

import org.junit.Test;
import ru.ohotNik.util.mysql.MySQL;

import static org.junit.Assert.assertTrue;
import static ru.ohotNik.util.mysql.query.condition.Conditions.fld;

/**
 * Created with IntelliJ IDEA.
 * User: OkhonchenkoAV
 * Date: 01.09.14
 * Time: 13:25
 */
@SuppressWarnings("UseOfSystemOutOrSystemErr")
public class DeleteTest {

	@Test
	public void testDelete() {
		System.out.println("--testDelete--");
		String queryTxt =
				MySQL.delete().from("test").toQuery().toString();
		System.out.println(queryTxt);
		assertTrue("Проверьте правильность запроса", queryTxt.contains("query=delete from `test`"));
	}

	@Test
	public void testDeleteWithCondition() {
		System.out.println("--testDeleteWithCondition--");
		String queryTxt =
				MySQL.delete().from("test")
						.where(fld("field2").eq("value"))
						.toQuery().toString();
		System.out.println(queryTxt);
		assertTrue("Проверьте правильность запроса", queryTxt.contains("query=delete from `test` where `field2`=\"value\""));
	}
}
