package ru.ohotNik.util.mysql.query;

import org.junit.Test;
import ru.ohotNik.util.mysql.MySQL;
import ru.ohotNik.util.mysql.query.common.SeparatedList;

import static org.junit.Assert.assertTrue;

/**
 * Created with IntelliJ IDEA.
 * User: OkhonchenkoAV
 * Date: 01.09.14
 * Time: 13:25
 */
@SuppressWarnings("UseOfSystemOutOrSystemErr")
public class InsertTest {

	@SuppressWarnings({"HardCodedStringLiteral", "DuplicateStringLiteralInspection"})
	@Test
	public void testInsert() {
		System.out.println("--testInsert--");
		SeparatedList fields = new SeparatedList();
		fields.addField("field1", "field2", "field3");
		fields.addField("field4");
		SeparatedList values = new SeparatedList();
		values.addBool(Boolean.TRUE);
		values.addInt(1);
		values.addNull();
		values.addString("string");

		String queryTxt =
				MySQL.insert().into("test").fields(fields).values(values).toQuery().toString();
		System.out.println(queryTxt);
		assertTrue("Проверьте правильность запроса", queryTxt.contains("query=insert into `test`" +
				"(`field1`,`field2`,`field3`,`field4`) VALUES (TRUE,1,null,\"string\")"));
	}
}
