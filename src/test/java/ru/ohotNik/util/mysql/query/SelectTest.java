package ru.ohotNik.util.mysql.query;

import org.junit.Test;
import ru.ohotNik.util.mysql.MySQL;

import static org.junit.Assert.assertTrue;
import static ru.ohotNik.util.mysql.query.condition.Conditions.fld;

/**
 * Created with IntelliJ IDEA.
 * User: OkhonchenkoAV
 * Date: 01.09.14
 * Time: 13:25
 */
@SuppressWarnings("UseOfSystemOutOrSystemErr")
public class SelectTest {

	@Test
	public void testSelect() {
		System.out.println("--testSelect--");
		String queryTxt =
				MySQL.select().from("test").toQuery().toString();
		System.out.println(queryTxt);
		assertTrue("Проверьте правильность запроса", queryTxt.contains("query=select * from `test`"));
	}

	@Test
	public void testSelectField() {
		System.out.println("--testCountField--");
		String queryTxt =
				MySQL.select("field").from("test").toQuery().toString();
		System.out.println(queryTxt);
		assertTrue("Проверьте правильность запроса", queryTxt.contains("query=select `field` from `test`"));
	}

	@Test
	public void testSelectFieldWithCond() {
		System.out.println("--testCountFieldWithCond--");
		String queryTxt =
				MySQL.select("field").from("test")
						.where(fld("field2").eq("value"))
						.toQuery().toString();
		System.out.println(queryTxt);
		assertTrue("Проверьте правильность запроса", queryTxt.contains("Query:\n" +
				"query=select `field` from `test` where  field2=\"value\""));
	}
}
