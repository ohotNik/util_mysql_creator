package ru.ohotNik.util.mysql.query;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import ru.ohotNik.util.mysql.MySQL;

/**
 * Created by ohotNik
 * Date : 08.10.14
 * Time : 21:01
 * Description :
 */
public class TableTest {

  @SuppressWarnings("HardCodedStringLiteral")
  @Test
	@Ignore
	public void testTableCrete() {
    MySQL.createTable("table");

    String excepted = "CREATE TABLE IF NOT EXISTS `table` (\n" +
        "`id` int(11) NOT NULL AUTO_INCREMENT,\n" +
        "`login` varchar(50) CHARACTER SET utf8 NOT NULL,\n" +
        "`email` varchar(50) CHARACTER SET utf8 NOT NULL,\n" +
        "`password` varchar(50) CHARACTER SET utf8 NOT NULL,\n" +
        " PRIMARY KEY (`id`),\n" +
        " UNIQUE KEY `name` (`login`,`email`)\n" +
        ") ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=1";

    Assert.assertEquals("Ошибка в составлении запроса на создание таблиц",
        "", excepted);

  }

}
